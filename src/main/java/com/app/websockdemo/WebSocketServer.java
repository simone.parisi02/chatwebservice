/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.websockdemo;

import com.app.wssdemo.*;
import java.io.IOException;
import java.io.StringReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.json.spi.JsonProvider;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import javax.websocket.server.PathParam;

/**
 *
 * @author Arturo
 */
@ApplicationScoped
@ServerEndpoint("/demows/{username}")
public class WebSocketServer {
    
    //Group List
    @ApplicationScoped static private ArrayList<Group> groups = new ArrayList<>();
    @ApplicationScoped static private ArrayList<User> users = new ArrayList<>();
    @ApplicationScoped static private ArrayList<Chat> chats = new ArrayList<>();
    @Inject
    SessionHandler sessions;
    
    
    @OnOpen
    public void onOpen(Session session, @PathParam("username") String username) throws IOException {
        sessions.add(session);
        JsonProvider json = JsonProvider.provider();
        JsonObject obj = json.createObjectBuilder()
               .add("type","system")
               .add("msg", username)
               .build();
        boolean find = false;
       
        for(User user : users){
            if(user.getName().equals(username)){
               find = true;               
            }
        }
       
        if(find){          
           session.close();
        } else {          
            JsonObject listOfUsers = json.createObjectBuilder()
               .add("type","listUsers")
               .add("msg", users.toString())
               .build();
            sendMessage( listOfUsers, session);
            //--------------------------
            this.users.add(new User(username, session));
            sendToAll(obj);
            //--------------------------
        }
    }
    
    private void sendMessage(JsonObject obj, Session s) {
        try {     
            s.getBasicRemote().sendText(obj.toString());
        } catch (IOException ex) {
            Logger.getLogger(WebSocketServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void sendToAll(JsonObject obj){
        for (Session s: sessions.getSessions()){
            if (s.isOpen()){
                sendMessage(obj,s);
            }
        }
    }

    @OnMessage
    public void onMessage(String msg, Session s) {
        JsonReader reader = Json.createReader(new StringReader(msg));
        JsonObject jsonMsg = reader.readObject();
        if ("user".equals(jsonMsg.getString("type"))){
            Session userToSend;
            String user2 = jsonMsg.getString("user2");
            String user1 = jsonMsg.getString("user1");
            for(User u : users){
                if(user2.equals(u.getName()))
                userToSend = u.getSession();
            }
            for(Chat c : chats){             
                if(user1.equals(c.user1.getName()) && user2.equals(c.user2.getName()) ||
                        user2.equals(c.user1.getName()) && user1.equals(c.user2.getName())){
                    System.out.println("Messaggio: " + jsonMsg.getString("msg"));
                    c.messages.add(new Message(new User(user1, s) ,jsonMsg.getString("msg")));
                }
            }
            
            sendToAll(jsonMsg);
        } else if("chat".equals(jsonMsg.getString("type"))){
            String user1 = jsonMsg.getString("user1");
            String user2 = jsonMsg.getString("user2");
            boolean find = false;
            for(Chat c : chats){
                
                System.out.println(user1);
                System.out.println(user2);
                System.out.println(c.user1.getName());
                System.out.println(c.user2.getName());
                
                if(user1.equals(c.user1.getName()) && user2.equals(c.user2.getName()) || 
                        user2.equals(c.user1.getName()) && user1.equals(c.user2.getName())){
                    System.out.println("Trovato");
                    ArrayList<Message> messages = c.messages;
                    JsonProvider json = JsonProvider.provider();                   
                    JsonArrayBuilder membersArray = Json.createArrayBuilder();
                    for (Message m : c.messages) {
                      membersArray.add(m.message + ","+m.user.getName());
                    }                   
                    JsonObject obj = json.createObjectBuilder()
                        .add("type","chat")
                        .add("messages", membersArray.build())
                        .build();
                    System.out.println(obj.asJsonObject());
                    sendMessage(obj, s);
                    find = true;
                    break;
                }
            }
            if(!find){
                Session otherSession = null;
                for(User u : users){
                    if(user2.equals(u.getName()))
                    otherSession = u.getSession();
                }
                chats.add(new Chat(new User(user1, s), new User(user2, otherSession)));
                System.out.println("Sessione aggiunta");
            }
            
        } 
    }
}
