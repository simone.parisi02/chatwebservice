package com.app.websockdemo;
/**
 *
 * @author spari
 */
import javax.websocket.Session;

public class User {
    
    private String name;
    private Session session;
    
    public User(String name, Session session){
        this.name = name;
        this.session = session;
    }
    
    public User(User user){
        this.name = user.name;
        this.session = user.session;
    }
    
    public Session getSession(){
        return this.session;
    }
    
    public String getName() { return name;}
    
    public void setName(String name) {this.name = name;}
    
    public String toString(){
        return name;
    }
}
