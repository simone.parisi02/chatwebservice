/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.wssdemo;

import com.app.websockdemo.*;
import java.util.ArrayList;

/**
 *
 * @author spari
 */
public class Chat {
    public User user1;
    public User user2;
    public ArrayList<Message> messages = new ArrayList();
    
    public Chat(User user1, User user2){
        this.user1 = user1;
        this.user2 = user2;
    }
}
