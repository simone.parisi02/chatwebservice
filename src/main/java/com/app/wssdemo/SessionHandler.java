/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.wssdemo;

import java.util.HashSet;
import java.util.Set;
import javax.enterprise.context.ApplicationScoped;
import javax.websocket.Session;

/**
 *
 * @author Simone
 */
@ApplicationScoped
public class SessionHandler {
    Set<Session> sessions;

public SessionHandler() {
    sessions = new HashSet<>();
}
public void add(Session s)  {
    sessions.add(s);
}

public void remove(Session s){
    sessions.remove(s);
}

public int size(){
    return sessions.size();
}
public Set<Session> getSessions() {
    return sessions;
}    
}
