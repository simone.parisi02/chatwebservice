/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.wssdemo;
import com.app.websockdemo.User;
import java.util.HashMap;

/**
 *
 * @author spari
 */
public class Group {
    
    private String groupName; 
    private HashMap<User, Boolean> users = new HashMap<>();
    
    public Group(String groupName){
        this.groupName = groupName;
    }
    
    public void addUser(User user, boolean isAdmin){
        users.put(user, isAdmin);
    }
    
    public HashMap<User, Boolean> getUsers(){
        return users;
    }
    
    public boolean getUserIsAdmin(User userKey){
        return users.get(userKey);
    }
    
    public String getGroupName(){
        return groupName;
    }
    
    public void setGroupName(String groupName){
        this.groupName = groupName;
    }
}
