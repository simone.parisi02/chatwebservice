/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.wssdemo;

import com.app.websockdemo.*;

/**
 *
 * @author spari
 */
public class Message {
    public User user;
    public String message;
    
    public Message(User user, String message){
        this.message = message;
        this.user = user;
    }
    
    
}
