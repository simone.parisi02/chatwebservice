
var username;
var socket;
var users;
var currentChatUser;
var usersList;

$(document).ready(() => 
    swal("Select an username:", {
        content: "input",
    }).then((value) => {
        username = value;  
        if(value == null || value == ""){
            location.reload();
        } else {
            startSocket();
        }           
    })   
);

function startSocket(){
   /* swal("Good job!", "Click to start your chat!", "success"); */
   socket = new WebSocket(`ws://localhost:8080/WebChatMaeven/demows/${username}`);
   socket.onmessage = onMessage; 
   socket.onclose = onClose;
}

function onMessage(event) {
    
    var msg = JSON.parse(event.data);

    if (msg.type === "system") {
        var newMsg;
        if(msg.msg === username){
            newMsg = `<div class="info-div yourName-div">${msg.msg}</div>`;
        } else {
            newMsg = `<div class="info-div" onclick="openChat(this);" id="${msg.msg}">${msg.msg}</div>`;
        }
        document.getElementById("users").innerHTML += newMsg;
    }
    
    if (msg.type === "user") {
        var newMsg=document.getElementById("msgContainer").innerHTML;
        if(msg.user1 === username){
            /* var newMsg= `<div class="msg my-msg">${msg.sendBy}: ${msg.msg}</div>`; */
            var newMsg= `<div class="msg my-msg">${msg.msg}</div>`;
        } else {
            var newMsg= `<div class="msg">${msg.sendBy}: ${msg.msg}</div>`;
        }       
        document.getElementById("msgContainer").innerHTML += newMsg;
    }

    if (msg.type === "chat") {
        console.log(msg);
        for(var i = 0; i < msg.messages.length; i++){
            tokens = msg.messages[i].split(',');
            console.log(username + '  ' + tokens[0]);
            if(tokens[1] === username){
                /* var newMsg= `<div class="msg my-msg">${msg.sendBy}: ${msg.msg}</div>`; */
                var newMsg= `<div class="msg my-msg">${tokens[0]}</div>`;
            } else {
                var newMsg= `<div class="msg">${tokens[1]}: ${tokens[0]}</div>`;
            }       
            document.getElementById("msgContainer").innerHTML += newMsg;
        }
    }

    if (msg.type === "listUsers") {
        var msgSub = msg.msg.substring(1, msg.msg.length-1);
        usersList = msgSub.split(", ");
        for(var i = 0; i < usersList.length; i++){
            var newMsg = `<div class="info-div" onclick="openChat(this);" id="${usersList[i]}">${usersList[i]}</div>`;          
            document.getElementById("users").innerHTML += newMsg;
        }
    }   
}

function inviaMsg() {
    var msg = document.getElementById("msg").value;
    if(msg != ""){
        document.getElementById("msg").value = "";
        var msgObj = {
            type:'user',      
            msg:msg,
            user1:username,
            user2:currentChatUser
        }
        socket.send(JSON.stringify(msgObj)); 
    }
}

function sendRequestMessages() {   
    var msgObj = {
        type:'chat',      
        user1:username,
        user2:currentChatUser,
    }
    socket.send(JSON.stringify(msgObj)); 
}

function onClose() {
    swal("Username is already in use:", {
        content: "input",
        inputPlaceholder: 'Username'
    }).then((value) => {
        username = value;  
        if(value == null || value == ""){
            location.reload();
        } else {
            startSocket();
        }           
    })  
}

function openChat(item) {
    this.currentChatUser = $(item).attr("id");
    console.log($(item).attr("id"));
    document.getElementById("msgContainer").innerHTML = "";
    sendRequestMessages();
}